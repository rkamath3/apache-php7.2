FROM nimmis/apache:latest

MAINTAINER Ram <aramachandrakamath@gmail.com>

# disable interactive functions
ENV DEBIAN_FRONTEND noninteractive

RUN LC_ALL=C.UTF-8 add-apt-repository ppa:ondrej/php && apt-get update && \
apt-get install -y \
    php7.2 \
    php7.2-cli \
    php7.2-common \
    php7.2-curl \
    php7.2-gd \
    php7.2-json \
    php7.2-mbstring \
    php7.2-intl \
    php7.2-mysql \
    php7.2-xml \
    php7.2-zip  \
    php7.2-memcached && \
    a2enmod php7.2 && \
rm -rf /var/lib/apt/lists/* && \
cd /tmp && curl -sS https://getcomposer.org/installer | php && \
mv composer.phar /usr/local/bin/composer && \
a2enmod rewrite && a2enmod ssl && a2ensite default-ssl
